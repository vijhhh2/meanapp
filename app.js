const express = require('express');
const path = require('path');
const bodyparser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');

const users = require('./routes/users');
const config = require('./config/database');

mongoose.connect(config.database);
mongoose.connection.on('connected', () => {
    console.log('db is connected '+ config.database);
});
mongoose.connection.on('Error', (err) => {
    console.log('db is connected '+ err);
});

const app = express();
const port = 3000;

app.use(cors());
app.use(express.static(path.join(__dirname,'public')));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));

//passport middleware
app.use(passport.initialize());
app.use(passport.session());
require('./config/passport')(passport);
//passport middleware

app.use('/users', users);

app.get('/', (req,res) => {
    res.send('INVALID ENDPOINT');
});

app.listen(port, () => {
    console.log(`port started on ${port}`);
});