const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');



router.post('/register', (req,res,next) => {
    let newUser = new User({
        name: req.body.name,
        email: req.body.email,
        username: req.body.username,
        password: req.body.password
    });
    User.addUser(newUser, (err,user) => {
        if(err) {
            res.status(400).json({success: false, msg: 'Fail to register user'});
        }
        if(user) {
            res.status(200).json({success: true, msg: 'user registered'});
        } else {
            res.status(400).json({success: false, msg: 'Fail to register user'});
        }
    });
});
router.post('/authenticate', (req,res,next) => {
    const username = req.body.username;
    const password = req.body.password;
    User.getUserByUsername(username, (err, user) => {
        // console.log(user);
        if(err) throw err;
        if(!user) {
            return res.json({success: false, msg: 'user not registered'});
        }
        User.comparepassword(password, user.password, (err, isMatch) => {
            if(err) throw err;
            // console.log(isMatch);
            if(isMatch) {
                const token = jwt.sign(user.toJSON(), config.secret, {
                    expiresIn: 604800
                });
                // console.log(token);
             res.json({
                success: true,
                msg: 'user authenticated',
                token: 'jwt '+token,
                user: {
                    id: user._id,
                    name: user.name,
                    username: user.username,
                    email: user.email
                }
            });
            } else {
                return res.json({success: false, msg: 'authentication failed'});
            }
        });
    });
});
router.get('/profile',passport.authenticate('jwt', {session: false}), (req,res,next) => {
    res.json({user: req.user});
});

module.exports = router;